module.exports = {
	env: {
		browser: true,
		es2021: true,
		jest: true
	},
	extends: 'eslint:recommended',
	overrides: [],
	parserOptions: {
		ecmaVersion: 'latest',
		sourceType: 'module',
	},
	rules: {
		'indent': ['error', 'tab'],
		'linebreak-style': ['error', 'unix'],
		'quotes': ['error', 'single'],
		'semi': ['error', 'always'],
		'no-console': 'off',
		'no-restricted-syntax': 'error',
		'no-mixed-spaces-and-tabs': 0,
		'no-with': 'error',
		'no-useless-escape': 'error',
		'no-extra-semi': 'error',
		'no-empty': 'error',
	},
};
