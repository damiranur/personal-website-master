const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
	entry: './src/main.js',
	output: {
		filename: 'bundle.js',
		clean: true,
		path: path.resolve(__dirname, 'build'),
	},
	mode: 'production',
	devtool: 'inline-source-map',
	devServer: {
		proxy: {
			'/api':'http://localhost:3000',
		},
		static: {
			directory: path.join(__dirname, 'build'),
		},
		compress: true,
		port: 8080,
		hot: true,
	},
	performance: {
		hints: false,
		maxEntrypointSize: 512000,
		maxAssetSize: 512000,
	},

	plugins: [
		new HtmlWebpackPlugin({
			filename: 'index.html',
			inject: true,
			template: path.resolve(__dirname, 'src', 'index.html'),
		}),
		new CopyPlugin({
			patterns: [{from: './src/assets/images', to: 'assets/images'}],
		}),
	],
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules)/,
				use: {
					loader: 'babel-loader',
				},
			},
			{
				test: /\.css$/i,
				use: ['style-loader', 'css-loader'],
			},
		],
	},
};
