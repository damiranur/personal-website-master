async function postData(url, data){
	const response = await fetch(url, {
		method: 'POST',
		body: JSON.stringify(data),
		headers: {
			'Content-Type': 'application/json',
		},
	});
	return {status: response.status, data: response.json()};
}

async function getData(url){
	const response = await fetch(url, {
		method: 'GET',
	});
	return response.json();
}

export async function subscribe(email){
	const data = await postData('http://localhost:3000/subscribe', {email});
	return data;
}

export async function unsubscribe(){
	const data = await postData('http://localhost:3000/unsubscribe');
	return data;
}

export async function getCommunity(){
	const data = await getData('http://localhost:3000/community');
	return data;
}