class WebsiteSection extends HTMLElement {
	constructor(){
		super();
	}
}
window.customElements.define('section-one', WebsiteSection);


const sectionOne = document.createElement('section-one');
sectionOne.className = 'app-section app-section--image-overlay app-section--image-peak';
const overlayImg = document.createElement('img');
overlayImg.className = 'app-logo';
overlayImg.src = './assets/images/your-logo-here.png';
overlayImg.alt = 'Logo icon picture';
overlayImg.loading = 'lazy';
sectionOne.appendChild(overlayImg);

const title = document.createElement('h1');
title.className = 'app-title';
title.innerHTML = 'Your Headline <br> Here';
sectionOne.appendChild(title);

const description  = document.createElement('h2');
description.className = 'app-subtitle';
description.innerHTML = 'Lorem ipsum dolor sit amet, consectetur <br> adipiscing elit sed do eiusmod.';
sectionOne.appendChild(description);

const appContainer = document.getElementById('app-container');
const footer = document.querySelector('.app-footer');

export const createSectionOne = () =>{
	appContainer.insertBefore(sectionOne, footer);
};

const sectionTwo = document.createElement('section-two');
sectionTwo.className = 'app-section';
const sectionTwoTitle = document.createElement('h2');
sectionTwoTitle.className = 'app-title';
sectionTwoTitle.innerHTML = 'This is the Section Headline, <br> Continues to Two Lines';
sectionTwo.appendChild(sectionTwoTitle);

const sectionTwoDesc = document.createElement('h3');
sectionTwoDesc.className = 'app-subtitle';
sectionTwoDesc.innerHTML = 'Lorem ipsum dolor sit amet, consectetur <br> adipiscing elit sed do eiusmod.';
sectionTwo.appendChild(sectionTwoDesc);

const sectionTwoContent = document.createElement('article');
sectionTwoContent.className = 'app-section__article';
sectionTwoContent.innerHTML = 
`<p>
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis.
</p>
<p>
Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</p>`;
sectionTwo.appendChild(sectionTwoContent);

const sectionTwoBtn = document.createElement('button');
sectionTwoBtn.className = 'app-section__button app-section__button--read-more';
sectionTwoBtn.innerHTML = 'Read more';
sectionTwo.appendChild(sectionTwoBtn);

export const createSectionTwo = () =>{
	appContainer.insertBefore(sectionTwo, footer);
};

const sectionThree = document.createElement('section-three');
sectionThree.className = 'app-section app-section--image-culture';

const sectionThreeTitle = document.createElement('h2');
sectionThreeTitle.className = 'app-title';
sectionThreeTitle.innerHTML = 'Learn more <br> about our culture...';
sectionThree.appendChild(sectionThreeTitle);

const sectionThreeBtn = document.createElement('button');
sectionThreeBtn.className = 'app-section__button app-section__button--our-culture';
sectionThreeBtn.innerHTML = '&#9654;';
sectionThree.appendChild(sectionThreeBtn);

const sectionThreeDesc = document.createElement('h3');
sectionThreeDesc.className = 'app-subtitle';
sectionThreeDesc.innerHTML = 'Duis aute irure dolor in reprehenderit <br> in voluptate velit esse cillum dolore eu <br> fugiat nulla';
sectionThree.appendChild(sectionThreeDesc);

export const createSectionThree = () =>{
	appContainer.insertBefore(sectionThree, footer);
};