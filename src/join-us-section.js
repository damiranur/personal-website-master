import { getCommunity, subscribe, unsubscribe } from './api.js';
import { validate } from './email-validator.js';
class Title {
	constructor(tagName, className, content) {
		const joinSectionTitle = document.createElement(tagName);
		joinSectionTitle.className = className;
		joinSectionTitle.innerHTML = content;
		this.node = joinSectionTitle;
	}
}
class Btn extends Title {
	constructor(tagName, className, content) {
		super(tagName, className, content);
		const subscribeBtn = document.createElement(tagName);
		subscribeBtn.className = className;
		this.type = 'submit';
		subscribeBtn.innerHTML = content;
	}
}

class SectionCreator {
	create(type) {
		switch (type) {
		case 'standard':
			// eslint-disable-next-line no-case-declarations
			const titleStandard = new Title('h2', 'app-title', 'Join Our Program')
				.node;
			// eslint-disable-next-line no-case-declarations
			const btnStandard = new Btn(
				'button',
				'app-section__button joinSection-subscribeBtn',
				'Subscribe',
				'submit',
			).node;
			return {titleStandard, btnStandard};

		case 'advanced':
			// eslint-disable-next-line no-case-declarations
			const titleAdvanced = new Title(
				'h2',
				'app-title',
				'Join Our Advanced Program',
			).node;
			// eslint-disable-next-line no-case-declarations
			const btnAdvanced = new Btn(
				'button',
				'app-section__button joinSection-subscribeBtn',
				'Subscribe to Advanced Program',
				'submit',
			).node;
			return {titleAdvanced, btnAdvanced};
		}
	}
}
const sectionCreator = new SectionCreator();

export const onloadStandardSection = () => {
	const createdJoinSection = createJoinSection();
	createStandardSection(
		createdJoinSection.joinSection,
		createdJoinSection.subscribeForm,
	);
};

const createStandardSection = (joinSection, subscribeForm) => {
	const standardTitle = sectionCreator.create('standard').titleStandard;
	const standardBtn = sectionCreator.create('standard').btnStandard;
	if (localStorage.getItem('buttonName') === 'Unsubscribe'){
		standardBtn.innerHTML = 'Unsubscribe';
	}
	joinSection.prepend(standardTitle);
	subscribeForm.append(standardBtn);
};

export const onloadAdvancedSection = () => {
	const createdJoinSection = createJoinSection();
	createAdvancedSection(
		createdJoinSection.joinSection,
		createdJoinSection.subscribeForm,
	);
};

const createAdvancedSection = (joinSection, subscribeForm) => {
	const advancedTitle = sectionCreator.create('advanced').titleAdvanced;
	const advancedBtn = sectionCreator.create('advanced').btnAdvanced;
	joinSection.prepend(advancedTitle);
	subscribeForm.append(advancedBtn);
};

const createJoinSection = () => {
	const appContainer = document.getElementById('app-container');
	const footer = document.querySelector('.app-footer');
	const joinSection = document.createElement('section');
	joinSection.className = 'app-section__join-our-program';

	const joinSectionSubtitle = document.createElement('h3');
	joinSectionSubtitle.className = 'app-subtitle';
	joinSectionSubtitle.innerHTML =
    'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';

	const subscribeForm = document.createElement('form');
	subscribeForm.className = 'joinSectionForm';

	const inputEmail = document.createElement('input');
	inputEmail.type = 'email';
	inputEmail.className = 'joinSection-email';
	inputEmail.placeholder = 'Email';
	if(localStorage.getItem('inputStyle') === 'joinSection-email-hide'){
		inputEmail.classList.add('joinSection-email-hide');
	}

	joinSection.appendChild(joinSectionSubtitle);
	joinSection.appendChild(subscribeForm);
	subscribeForm.appendChild(inputEmail);

	appContainer.insertBefore(joinSection, footer);
	const handleSubmit = async(e) => {
		e.preventDefault();
		if (validate(e.target.elements[0].value)) {
			const subscribeBtn = document.querySelector('.joinSection-subscribeBtn');
			const emailInputElement = document.querySelector('.joinSection-email');
			let email = emailInputElement.value;
			subscribeBtn.disabled = true;
			subscribeBtn.style = 'opacity: 0.5';
			const {data, status} = await subscribe(email);
			subscribeBtn.disabled = false;
			subscribeBtn.style = 'opacity: 1';
			if (subscribeBtn.innerHTML === 'Subscribe'){
				if (status === 422){
					console.log((await data).error, status);
					const {error} = await data;
					alert(error);
				} else {
					subscribeBtn.innerHTML = 'Unsubscribe';
					localStorage.setItem('buttonName', subscribeBtn.innerHTML);
					emailInputElement.classList.add('joinSection-email-hide');
					localStorage.setItem('inputStyle', emailInputElement.classList );
				}
			} else {
				unsubscribe();
				subscribeBtn.innerHTML = 'Subscribe';
				localStorage.setItem('buttonName', subscribeBtn.innerHTML);
				emailInputElement.classList.add('joinSection-email');
				emailInputElement.classList.remove('joinSection-email-hide');
				localStorage.setItem('inputStyle', emailInputElement.classList );
				emailInputElement.value = '';
				localStorage.setItem('email', emailInputElement.value);
				
			}
		}
	};

	subscribeForm.addEventListener('submit', handleSubmit);
	
	const addEmailToLocalStorage = () => {
		localStorage.setItem('email', inputEmail.value);
	};
	inputEmail.addEventListener('input', addEmailToLocalStorage);
	localStorage.getItem('email');
	inputEmail.value = localStorage.getItem('email') || '';
	
	return {joinSection, subscribeForm};
};

export const createCommunitySection = async () =>{
	const appContainer = document.getElementById('app-container');
	const imgCultureSection = document.querySelector('section-three');
	const communitySection = document.createElement('section');
	communitySection.className = 'app-section app-section__community';
	
	const communityTitle = document.createElement('h2');
	communityTitle.className = 'app-title';
	communityTitle.innerHTML =
    'Big Community of People Like You ';

	const communitySubtitle = document.createElement('h3');
	communitySubtitle.className = 'app-subtitle';
	communitySubtitle.innerHTML =
    'We are proud of our products, and we are really excited when we get feedback from our users.';
	
	const feedbacksBox = document.createElement('div');
	feedbacksBox.className = ('feedbacks-box');

	const data = await getCommunity();
	data.forEach((el)=>{
		const feedbackCard = document.createElement('div');
		feedbackCard.className = ('feedback-card');
		feedbackCard.id = `${el.id}`;
		feedbacksBox.appendChild(feedbackCard);
		
		const userPhoto = document.createElement('img');
		userPhoto.className = ('user-photo');
		userPhoto.src = `${el.avatar}`;
		feedbackCard.appendChild(userPhoto);

		const feedbackText = document.createElement('div');
		feedbackText.className = ('feedback-text');
		feedbackText.innerHTML = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.';
		feedbackCard.appendChild(feedbackText);

		const userName = document.createElement('div');
		userName.className = ('user-name');
		userName.innerHTML = `${el.firstName} ${el.lastName}`;
		feedbackCard.appendChild(userName);

		const userPosition = document.createElement('div');
		userPosition.className = ('user-position');
		userPosition.innerHTML = `${el.position}`;
		feedbackCard.appendChild(userPosition);
	});

	communitySection.appendChild(communityTitle);
	communitySection.appendChild(communitySubtitle);
	communitySection.appendChild(feedbacksBox);
	
	appContainer.insertBefore(communitySection, imgCultureSection);
};
// createCommunitySection();


