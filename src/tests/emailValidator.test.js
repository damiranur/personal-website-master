import { assert, expect } from 'chai';
import { validate, validateAsync, validateWithLog, validateWithThrow } from '../email-validator';
require('mocha-sinon');

describe('first test', () => { 
	it('should return 2', () => {
		expect(2).to.equal(2);
	}); 
});

describe('validate function', ()=>{
	it('returns false if email input field is empty', ()=> {
		const expected = false;
		const actual = validate('');
		expect(actual).to.equal(expected);
	});
	it('returns false if email ends with @test.com', ()=> {
		const expected = false;
		const actual = validate('@test.com');
		expect(actual).to.equal(expected);
	});
	it('returns false if email ends with @123.12', ()=> {
		const expected = false;
		const actual = validate('@123.12');
		expect(actual).to.equal(expected);
	});
});

describe('validateAsync() function', ()=>{
	it('returns true asynchronously if the email contains a valid ending', async()=>{
		const expected = true;
		const actual = await validateAsync('@gmail.com'|| '@outlook.com');
		expect(actual).to.equal(expected);
	});
});

describe('validateWithThrow() function', ()=>{
	it('throws an error with a message that the provided email is invalid',()=>{
		assert.throws(() => validateWithThrow('@mail.com'), Error, 'provided email is invalid');
		
	});
});

describe('validateWithLog() function', () => {
    
	beforeEach(function() {
		this.sinon.stub(console, 'log');
	});
	it('should log true', ()=>{
		validateWithLog('@gmail.com');
		expect( console.log.calledOnce ).to.be.true;
		expect( console.log.calledWith('true') ).to.be.true;
	});
	it('should log false', ()=>{
		validateWithLog('@mail.com');
		expect( console.log.calledOnce ).to.be.true;
		expect( console.log.calledWith('false') ).to.be.true;
	});
});