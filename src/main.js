import { createCommunitySection } from './join-us-section';
import { onloadStandardSection } from './join-us-section.js';
import { measureFetchPerform } from './performance-metrics';
import './styles/normalize.css';
import './styles/style.css';
import { createSectionOne, createSectionThree, createSectionTwo } from './web-components';



window.onload = () => {
	createSectionOne();
	createSectionTwo();
	createSectionThree();
	createCommunitySection();
	onloadStandardSection();
	sendUserClicksToWorker();
	measureFetchPerform();
} ;


const sendUserClicksToWorker = () => {
	if (window.Worker) {
		const url = new URL('./web-worker.js', import.meta.url);
		const worker = new Worker(url);

		const allButtonsToStore = document.getElementsByClassName('app-section__button');		
		const inputField = document.getElementsByClassName('joinSection-email');	
		
		Array.from(allButtonsToStore).forEach(el => {
			el.addEventListener('click', function () {
				worker.postMessage(this.textContent);
			});
		});
		
		Array.from(inputField).forEach(element => {
			element.addEventListener('click', function () {
				worker.postMessage(this.type);
			});
		});

		worker.onmessage = e => {
			console.log('Thread received data: ', e.data);
			sendButtonClicksToServer(e.data);
		};
		worker.onerror = error => {
			console.log(error.message);
		};
	}

};


async function postData(url, data){
	const response = await fetch(url, {
		method: 'POST',
		body: JSON.stringify(data),
		headers: {
			'Content-Type': 'application/json',
		},
	});
	return {status: response.status, data: response.json()};
}

export async function sendButtonClicksToServer(dataToSend){
	const data = await postData('http://localhost:3000/analytics/user', {dataToSend});
	return data;
}

