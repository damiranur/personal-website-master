const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com'];

export const validate = (email) => {
	return VALID_EMAIL_ENDINGS.some((el)=>email.endsWith(el));
};

export default validate;

export const validateAsync = async (email)=>{
	return await VALID_EMAIL_ENDINGS.some((el)=>email.endsWith(el));
}; 

export const validateWithThrow = (email) => {
	!VALID_EMAIL_ENDINGS.includes(email.split('@')[1].split('.')[0]);
	throw new Error ('provided email is invalid');
};

export const validateWithLog = (email) => {
	for (let i = 0; i < VALID_EMAIL_ENDINGS.length; i += 1) {
		if (email.endsWith(VALID_EMAIL_ENDINGS[i])) {
			console.log('true');
			return true;
		}
	}
	console.log('false');
	return false;
};
