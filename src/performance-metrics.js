

export const measureFetchPerform = ()=>{
	console.log('object');
	const fetchCommunityStart = performance.now();
	fetch('http://localhost:3000/community').then(()=>{
		const fetchCommunityEnd = performance.now();
		const fetchTime = fetchCommunityEnd - fetchCommunityStart;
		const pageLoadTime = measurePageLoadSpeed();
		const memoryUsageTime = measurePageMemoryUsageTime();
		const performMetrics = {
			fetchTime,
			pageLoadTime,
			memoryUsageTime
		};
		console.log(performMetrics);

		fetch('http://localhost:3000/analytics/performance', {
			method: 'POST', 
			body: JSON.stringify(performMetrics),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(response => {
			console.log('Performance metrics sent to server with response ' + response.statusText);
		}).catch(error =>{
			console.log('Error sending performance metrics to server ', error);
		});	
	}).catch(() =>{
		console.log('error while fetching community for analytics error');
	});
    
	const measurePageMemoryUsageTime = () => {
		let usage;
		if ('memory' in window.performance){
			const memoryUsageTime  = performance.memory.usedJSHeapSize;
			console.log(`Memory usage: ${memoryUsageTime} bytes`);
			usage = memoryUsageTime;
		} 
		return usage;
	};

	const measurePageLoadSpeed =()=>{
		const pageLoadTime = window.performance.timing.loadEventEnd - window.performance.timing.navigationStart;
		console.log(`Page load time: ${pageLoadTime} milliseconds`);
		return pageLoadTime;
	};
};